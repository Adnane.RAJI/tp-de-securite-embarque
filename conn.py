import asyncio
from bleak import BleakClient

address = "D0:75:11:F6:A7:51"

async def main(address):
    client = BleakClient(address)
    try:
        await client.connect()
        services = await client.get_services()
        for service in services:
            print(f"Service UUID: {service.uuid}")
            for characteristic in service.characteristics:
                print(f"  Characteristic UUID: {characteristic.uuid}")
    except Exception as e:
        print(e)
    finally:
        await client.disconnect()

asyncio.run(main(address))