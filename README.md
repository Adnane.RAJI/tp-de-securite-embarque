**TP IOT :**

**1- le script pour récupérer le fichier sur le boitier :**


-apées avoir installer la version 3.10.0 de python on installe les bibliothèques Bleak et Asyncio (: pip install bleak pip install bleak).
-Installer NRF sur le téléphone ;
-Installer Hex Editor Neo : https://www.hhdsoftware.com/free-hex-editor

2-afficher les appareils disponible : asyn def afficher(): devices = await BleakScanner.discover() for d in devices: print(d) . 

3-pour trouver notre appareils on récupére l'adresse correspond au numero de notre boitiers . 

4-Se connecter au boitier : async def se_connecter(address): async with BleakClient(address, timeout=10000.0) as client: services = await client.get_services() . 

async def main():
try:
    device_address = "D0:75:11:F6:A7:51"
    await se_connecter(device_address)
except BleakError as e:
    print(f"Error occurred while scanning for devices: {e}")
    

asyncio.run(main())

5- Ecrire une fonction read_characteristics qui se connecte au boitier et qui récupérer le numéro de la charactéristique lue précedemment : 

async def read_characteristics(address):
    try:
        async with BleakClient(address, timeout=10000.0) as client:
            # success = await client.pair()
            # if success:
            #     print("Pairing successful!")
            # else:
            #     print("Pairing failed.")
            services = await client.get_services()

            for service in services:
                print(f"Service UUID: {service.uuid}")
                for characteristic in service.characteristics:
                    if characteristic.uuid == "1b0d1303-a720-f7e9-46b6-31b601c4fca1":
                        await client.start_notify("1b0d1303-a720-f7e9-46b6-31b601c4fca1", callback=my_notification_callback)
                        print(f"Characteristic UUID: {characteristic.uuid}")
                        data = await client.read_gatt_char(characteristic)
                        print(data)
                        integer_data = int.from_bytes(data, byteorder='little') 
                        print("Read data:", integer_data)
                        await client.start_notify("1b0d1302-a720-f7e9-46b6-31b601c4fca1", callback=my_notification_callback)
                        await client.write_gatt_char("1b0d1302-a720-f7e9-46b6-31b601c4fca1", data)
                        # Construct the data payload
                        name = b"T10\x00\x00\x00\x00\x00"
                        chunkId = b"\x00\x00\x00\x00"
                        numChunk = b"\x00\x00\x27\x0F"
                        Filerequest = name + chunkId + numChunk
                        #Filerequest_bytes = Filerequest.encode('utf-8')
                        await client.start_notify("1b0d1304-a720-f7e9-46b6-31b601c4fca1", callback=my_notification_callback)
                        await client.write_gatt_char("1b0d1304-a720-f7e9-46b6-31b601c4fca1", Filerequest)


            time.sleep(10)
                        

                
    except BleakError as e:
        print(f"Error occurred while connecting to {address}: {e}")


6- Ecrire la fonction qui permet d'afficher les notifications : 
async def my_notification_callback(characteristic, data):

    print(f"Received notification from {characteristic.uuid}: {data.hex()}")


7-Activer les notification pour les characteristiques de valeurs : NUM et READ

async def read_characteristics(address):
    try:
        se_connecter(address)
        for service in services:
            print(f"Service UUID: {service.uuid}")
            for characteristic in service.characteristics:
                if characteristic.uuid == "1b0d1303-a720-f7e9-46b6-31b601c4fca1":
                    await client.start_notify("1b0d1303-a720-f7e9-46b6-31b601c4fca1", callback=my_notification_callback)
                    print(f"Characteristic UUID: {characteristic.uuid}")
                    data = await client.read_gatt_char(characteristic)
                    print(data)
                    integer_data = int.from_bytes(data, byteorder='little') 
                    print("Read data:", integer_data)
                    await client.start_notify("1b0d1302-a720-f7e9-46b6-31b601c4fca1", callback=my_notification_callback)
                    await client.write_gatt_char("1b0d1302-a720-f7e9-46b6-31b601c4fca1", data)
                    
        time.sleep(10)
                    

            
    except BleakError as e:
        print(f"Error occurred while connecting to {address}: {e}")    


8-Dans l'outil Hew Editor Neo, rajouter le code suivant avec les valeurs affichées par les notifications :
Struct LIST{
        Char filename[8] ;
        U32 size ;
U32 hash ;
U64 creationdate ;} ;
List list @ 0x00 ;


9-Créer la payload qui sera ensuite passer à la charactéristique. La payload doit contenir le nom du fichier, le chuckID et le numChunk qui sont le résulat du code précédent :
                    name = b"T10\x00\x00\x00\x00\x00"
                    chunkId = b"\x00\x00\x00\x00"
                    numChunk = b"\x00\x00\x27\x0F"
                    Filerequest = name + chunkId + numChunk


10-Rajouter les lignes d'envoie de la payload et d'activation des notifications pour cette charactéristique dans la fonction read_characteristics:

await client.start_notify("1b0d1304-a720-f7e9-46b6-31b601c4fca1", callback=my_notification_callback)
                    await client.write_gatt_char("1b0d1304-a720-f7e9-46b6-31b601c4fca1", Filerequest)
